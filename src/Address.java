public class Address {
    private String division;
    private String district;
    private String area;

    public Address(String division, String district, String area) {
        this.division = division;
        this.district = district;
        this.area = area;
    }
}
