import java.security.PrivateKey;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Employee extends JDBC {

    private String employeePin;
    private String name;
    private String designation;
    private String phoneNumber;
    private Address address;
    private Double accountBalance;


    public Employee(String employeePin, String name, String designation, String phoneNumber, Address address, Double accountBalance) {
        this.employeePin = employeePin;
        this.name = name;
        this.designation = designation;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.accountBalance = accountBalance;
    }

    public String getEmployeePin() {
        return employeePin;
    }

    public void setEmployeePin(String employeePin) {
        this.employeePin = employeePin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Employee() {
    }

    void insertEmployee(List<Employee> employeeList) throws SQLException {

        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        String query = "TRUNCATE `company`.`employee`";
        statement.executeUpdate(query);

        for (Employee employee : employeeList) {

            String query2 = "INSERT INTO employee (empID,empName,designation,accountBalance) VALUES ('" + employee.employeePin + "','" + employee.name + "','" + employee.phoneNumber + "'," + employee.accountBalance + ")";
            statement.executeUpdate(query2);
            System.out.println("Employee Name: " + employee.name + ", " + "Employee PIN: " + employee.employeePin + " Successfully Inserted In Database.");

        }
    }
}
