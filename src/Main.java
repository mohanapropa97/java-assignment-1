import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {


        List<Employee> employeeList = Arrays.asList(
                new Employee("1", "propa", "Manager", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 1000d),
                new Employee("2", "mohana", "Assistant Manager", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 2000d),
                new Employee("3", "aradhya", "Business Analyst", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 3000d),
                new Employee("4", "pallab", "HR", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 4000d)
                /*new Employee("5", "pd", "Manager", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 5000d),
                new Employee("6", "mbp", "Business Analyst", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 6000d),
                new Employee("7", "Eyana", "HR", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 7000d),
                new Employee("8", "Adiba", "HR", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 8000d),
                new Employee("9", "Tawsif", "Assistant Manager", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 9000d),
                new Employee("10", "Farhan", "Manager", "123456789",
                        new Address("Dhaka", "Dhaka", "MDOHS"), 9500d)*/);

        new Employee().insertEmployee(employeeList);
        CompanyAccount com = new CompanyAccount();
        com.disburseSalary(employeeList);

    }
}