import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompanyAccount extends JDBC {
    Double currentBalance;
    Double baseSalary;
    Double hourlyRate;
    Double extraRate ;
    Double salary;
    int count=0;
    List<Double> rechargeAmountList= new ArrayList<>();
    Scanner in = new Scanner(System.in);


    Double calculateSalary(String designation) {
        if (designation.equals("Manager")) {
            baseSalary = 5000d;
        } else if (designation.equals("Assistant Manager")) {
            baseSalary = 4000d;
        } else if (designation.equals("Business Analyst")) {
            baseSalary = 3000d;
        } else if (designation.equals("HR")) {
            baseSalary = 2000d;
        }
        salary = baseSalary + (hourlyRate * extraRate);
        return salary;
    }

    void rechargeCompanyAccountBalance() {
        System.out.print("Enter recharge amount: ");
        Double rechargeAmount = in.nextDouble();
        if (rechargeAmount == 0) {
            System.out.println("Account balance cannot be zero\n");
            rechargeCompanyAccountBalance();
        } else if (rechargeAmount < 0) {
            System.out.println("Account balance cannot be negative\n");
            rechargeCompanyAccountBalance();
        } else {
            count++;
            //System.out.println("Before: "+currentBalance);
            currentBalance += rechargeAmount;
           // System.out.println("After: "+currentBalance);
            rechargeAmountList.add(rechargeAmount);
        }
    }

    void updateEmployeeDatabase(Employee employee) throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        String query = "UPDATE employee SET accountBalance = " + salary + " WHERE empID = '" + employee.getEmployeePin() + "'";
        statement.executeUpdate(query);
        System.out.println("EmployeePIN: "+employee.getEmployeePin() + " " + employee.getName() + "'s salary paid and successfully update in database.");
    }


    void disburseSalary(List<Employee> employeeList) throws SQLException {

        System.out.print("Enter company account balance: ");
        currentBalance = in.nextDouble();
        System.out.print("Enter Hourly Rate: ");
        hourlyRate = in.nextDouble();
        System.out.print("Enter extra rate: ");
        extraRate = in.nextDouble();

        for (Employee employee : employeeList) {
            salary = calculateSalary(employee.getDesignation());

            while (currentBalance < salary) {
                System.out.println("\nInsufficient company balance. Please recharge.\n");
                rechargeCompanyAccountBalance();
            }
            currentBalance = currentBalance - salary;
            salary+=employee.getAccountBalance();
            updateEmployeeDatabase(employee);

        }

        System.out.print("Enter the time to see recharge amount at that time: ");
        int time = in.nextInt();
        System.out.println("The recharge amount at "+ time+" time is: "+rechargeAmountList.get(time-1));

    }


}
